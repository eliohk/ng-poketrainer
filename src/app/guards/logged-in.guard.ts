import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageKey } from '../enum/storageKey.enum';
import { UserService } from '../services/user.service';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class LoggedInGuard implements CanActivate {

  constructor (private readonly userService : UserService) {

  }
  
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      if (StorageUtil.storageRead(StorageKey.User) != undefined &&
      StorageUtil.storageRead(StorageKey.User) != "") {
        return true;
      }

      return false;
    }
  
}
