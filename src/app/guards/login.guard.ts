import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageKey } from '../enum/storageKey.enum';
import { UserService } from '../services/user.service';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor (private readonly userService : UserService, private readonly router:Router) {

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      if (StorageUtil.storageRead(StorageKey.User) == undefined) {
        return true;
      }

      if (StorageUtil.storageRead(StorageKey.User) == "") {
        return true;
      }

      return false;
  }
  
}
