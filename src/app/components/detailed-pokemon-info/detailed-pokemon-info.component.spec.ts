import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedPokemonInfoComponent } from './detailed-pokemon-info.component';

describe('DetailedPokemonInfoComponent', () => {
  let component: DetailedPokemonInfoComponent;
  let fixture: ComponentFixture<DetailedPokemonInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailedPokemonInfoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DetailedPokemonInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
