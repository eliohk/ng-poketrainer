import { Component, Input } from '@angular/core';
import { PokemonDetails } from 'src/app/models/pokemon';

@Component({
  selector: 'app-detailed-pokemon-info',
  templateUrl: './detailed-pokemon-info.component.html',
  styleUrls: ['./detailed-pokemon-info.component.css']
})
export class DetailedPokemonInfoComponent {
  @Input() pokemon?: PokemonDetails
}
