import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiniTrainerProfileComponent } from './mini-trainer-profile.component';

describe('MiniTrainerProfileComponent', () => {
  let component: MiniTrainerProfileComponent;
  let fixture: ComponentFixture<MiniTrainerProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MiniTrainerProfileComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MiniTrainerProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
