import { Component, OnInit } from '@angular/core';
import { StorageKey } from 'src/app/enum/storageKey.enum';
import { UserService } from 'src/app/services/user.service';
import { StorageUtil } from 'src/app/utils/storage.util';

@Component({
  selector: 'app-mini-trainer-profile',
  templateUrl: './mini-trainer-profile.component.html',
  styleUrls: ['./mini-trainer-profile.component.css']
})
export class MiniTrainerProfileComponent {
  constructor(private readonly userService: UserService) {

  } 

  username? : string = this.userService.username;

  
}
