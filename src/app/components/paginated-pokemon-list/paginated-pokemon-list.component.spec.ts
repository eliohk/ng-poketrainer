import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginatedPokemonListComponent } from './paginated-pokemon-list.component';

describe('PaginatedPokemonListComponent', () => {
  let component: PaginatedPokemonListComponent;
  let fixture: ComponentFixture<PaginatedPokemonListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginatedPokemonListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PaginatedPokemonListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
