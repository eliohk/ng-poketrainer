import { Component, Output, Input, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon, PokemonDetails } from 'src/app/models/pokemon';
import { LoadedPokemon, Result } from 'src/app/models/pokemonList';
import { PokemonService } from 'src/app/services/pokemon.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-paginated-pokemon-list',
  templateUrl: './paginated-pokemon-list.component.html',
  styleUrls: ['./paginated-pokemon-list.component.css']
})
export class PaginatedPokemonListComponent {
  animation = false;
  constructor(private readonly userService:UserService, private readonly pokemonService:PokemonService) {

  }

  @Input() pokemon?:LoadedPokemon

  detailedPokeInfo?: PokemonDetails;

  ngOnInit(): void {

  }

  moreInfoSelected() {
    if (this.detailedPokeInfo == undefined) {
      this.getDetails(this.pokemon?.name!);
    } else {
      this.detailedPokeInfo = undefined;
    }
  }

  addToCollectionSelected() {
    this.savePokemon(this.pokemon?.name!, String(this.pokemon?.id!))
    this.animation = true;
  }

  savePokemon(pokemon: string, id:string) {
    this.userService.postPokemonToUser(pokemon, id).subscribe((data) => {
      this.userService.userPokemons = data.pokemons;
    })
  } 

  getDetails(pokemon: string) {
    this.pokemonService.getPokemonByName(pokemon).subscribe((data) => {
      let abilityArr: string[] = [];
      data.abilities.map((abilities, i) => {
        abilityArr.push(abilities.ability.name)
      });

      let typesArr: string[] = [];


      data.types.map((types, i) => {
          typesArr[types.slot - 1] = types.type.name;
      });

      this.detailedPokeInfo = {
        name: "Name: " + data.name,
        abilities: abilityArr,
        base_experience: "Base experience: " + String(data.stats[0].base_stat),
        height: "Height: " + String(data.height),
        stats: "Stats: " + String(data.stats[0].base_stat),
        types: typesArr,
        weight: "Weight: " + String(data.weight)
      };
    });
  }
}
