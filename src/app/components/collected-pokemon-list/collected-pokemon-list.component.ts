import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { CollectedPokemon } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-collected-pokemon-list',
  templateUrl: './collected-pokemon-list.component.html',
  styleUrls: ['./collected-pokemon-list.component.css']
})
export class CollectedPokemonListComponent implements OnInit {
  constructor(private readonly userService:UserService, private readonly route:Router) {

  }

  url?:string;

  ngOnInit(): void {
    this.url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+this.pokemon?.id+".png"
  }

  @Input() pokemon?:CollectedPokemon


  delete() {
    this.userService.deletePokemonFromUser({username: this.pokemon?.username!, id:this.pokemon?.id!}).subscribe((data) => {
      this.userService.userPokemons = data.pokemons;
      this.pokemon = undefined;
    })
  }

}
