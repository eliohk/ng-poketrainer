import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectedPokemonListComponent } from './collected-pokemon-list.component';

describe('CollectedPokemonListComponent', () => {
  let component: CollectedPokemonListComponent;
  let fixture: ComponentFixture<CollectedPokemonListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollectedPokemonListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CollectedPokemonListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
