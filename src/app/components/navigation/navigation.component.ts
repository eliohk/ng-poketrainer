import { Component, OnInit } from '@angular/core';
import { StorageKey } from 'src/app/enum/storageKey.enum';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { StorageUtil } from 'src/app/utils/storage.util';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit{

  constructor (private readonly userService : UserService) {

  }
  ngOnInit(): void {
  }

  get user():User{
    return this.userService.user
  }

}
