
    export interface Result {
        name: string;
        url: string;
    }

    export interface PokemonList {
        count: number;
        next: string;
        previous?: any;
        results: Result[];
    }

    export interface LoadedPokemon {
        id: number, 
        name: string;
        avatar: string
    }
    

