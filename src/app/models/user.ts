import { Pokemon } from "./pokemon"

export interface User {
    id: string,
    username: string, 
    pokemons: CollectedPokemon[]
}

export interface PostUser {
    username: string, 
    pokemons: string[]
}

export interface CollectedPokemon {
    id: string,
    username: string
}

