export class StorageUtil {
    public static storageSave<T>(key: string, value: T): void {
        sessionStorage.setItem(key, JSON.stringify(value))
    }

    public static storageRemove<T>(key: string) :void {
        sessionStorage.removeItem(key);
    }

    public static storageRead<T>(key: string): T | undefined {
        const data = sessionStorage.getItem(key)

        if (data){
            return JSON.parse(data) as T
        }
        return undefined
    }
}