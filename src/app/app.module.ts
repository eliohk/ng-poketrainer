import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPage } from './pages/login/login.page';
import { PokemonPage } from './pages/pokemon/pokemon.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { NavigationComponent } from './components/navigation/navigation.component';
import { MiniTrainerProfileComponent } from './components/mini-trainer-profile/mini-trainer-profile.component';
import { CollectedPokemonListComponent } from './components/collected-pokemon-list/collected-pokemon-list.component';
import {HttpClientModule} from '@angular/common/http';
import { PaginatedPokemonListComponent } from './components/paginated-pokemon-list/paginated-pokemon-list.component'
import { FormsModule } from '@angular/forms';
import { DetailedPokemonInfoComponent } from './components/detailed-pokemon-info/detailed-pokemon-info.component'
@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    PokemonPage,
    TrainerPage,
    NavigationComponent,
    MiniTrainerProfileComponent,
    CollectedPokemonListComponent,
    PaginatedPokemonListComponent,
    DetailedPokemonInfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
