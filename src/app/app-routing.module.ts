import { NgModule } from '@angular/core';
import { LoginPage } from "./pages/login/login.page";
import { PokemonPage } from "./pages/pokemon/pokemon.page";
import { TrainerPage } from "./pages/trainer/trainer.page";
import { RouterModule, Routes } from '@angular/router';
import { LoginGuard } from './guards/login.guard';
import { LoggedInGuard } from './guards/logged-in.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: LoginPage,
    canActivate: [LoginGuard]
  },
  {
    path: 'trainer',
    component: TrainerPage,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'pokemon',
    component: PokemonPage,
    canActivate: [LoggedInGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
