import { Injectable } from '@angular/core';
import { Observable, switchMap, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Pokemon } from '../models/pokemon';
import { PokemonList } from '../models/pokemonList';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private readonly http:HttpClient) { 
    
  }

  getPokemon(url: string) : Observable<PokemonList> {
    if (url === "") {
      return this.http.get<PokemonList>('https://pokeapi.co/api/v2/pokemon?limit=10&offset=0');
    }

    return this.http.get<PokemonList>(url)
  };

  getPokemonByName(name: string) : Observable<Pokemon> {
    return this.http.get<Pokemon>(`https://pokeapi.co/api/v2/pokemon/${name}`)
  }
}
