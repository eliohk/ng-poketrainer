import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, Observable } from 'rxjs';
import { StorageKey } from '../enum/storageKey.enum';
import { Pokemon } from '../models/pokemon';
import { CollectedPokemon, PostUser, User } from '../models/user';
import { StorageUtil } from '../utils/storage.util';
import { apiKey } from '../environment/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // _user :User = {
  //   username: "",
  //   pokemons: []
  // };


  private _user?: User = {
    id: "-1",
    username: "",
    pokemons: []
  }

  constructor(private readonly http: HttpClient) {

  }

  public getUser(username: string): Observable<User[]> {
    return this.http.get<User[]>(`https://snapdragon-reliable-badge.glitch.me/trainers?username=${username}`)   
  }

  set id(data: string) {
    this._user!.id = data;
  }

  set userName(data: string) {
    StorageUtil.storageSave(StorageKey.User, data)
    this._user!.username = data;
  }

  set userPokemons(data: CollectedPokemon[]) {
    this._user!.pokemons = data;
  }

  get id():string {
    return this._user?.id!;
  }

  get username():string {
    return this._user!.username;
  }

  get user():User {
    return this._user!;
  }

  get pokemons():CollectedPokemon[] {
    return this._user?.pokemons!;
  }

  public createUser(username: string): Observable<User>{
    const user  = {
      username: username,
      pokemons: []
    }
    const url = "https://snapdragon-reliable-badge.glitch.me/trainers"

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        "X-API-KEY": apiKey
      })
    };

    return this.http.post<User>(url, user, httpOptions).pipe(
    );
  }

  postPokemonToUser(pokemon: string, id: string) : Observable<User> {
    let tempArr : CollectedPokemon[] = [];

    for (let i = 0; i < this.pokemons.length; i++) {
      tempArr.push(this.pokemons[i])
    }

    tempArr.push({id: id, username:pokemon})

    let tempUser: User = {
      id: this.id,
      username: this.username,
      pokemons: tempArr
    }

    const url = `https://snapdragon-reliable-badge.glitch.me/trainers/${tempUser.id}`

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        "X-API-KEY": apiKey
      })
    };

    return this.http.patch<User>(url, tempUser, httpOptions);
  }

  deletePokemonFromUser(pokemon:CollectedPokemon) : Observable<User> {
    let tempArr : CollectedPokemon[] = [];

    let deletedYes : boolean = true;

    for (let i = 0; i < this.pokemons.length; i++) {
      if (this.pokemons[i].username == pokemon.username && deletedYes) {
        deletedYes = false;
        continue;
      }
      tempArr.push(this.pokemons[i])
    }

    let tempUser: User = {
      id: this.id,
      username: this.username,
      pokemons: tempArr
    }

    const url = `https://snapdragon-reliable-badge.glitch.me/trainers/${tempUser.id}`

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        "X-API-KEY":  apiKey
      })
    };

    return this.http.patch<User>(url, tempUser, httpOptions).pipe(
    );
  }

  logOut() {
    StorageUtil.storageRemove(StorageKey.User);
    this.id = "-1";
    this.userPokemons = [];
    this.userName = "";
  }

}
