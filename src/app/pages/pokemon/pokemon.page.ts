import { Component, OnInit } from '@angular/core';
import { LoadedPokemon, Result } from 'src/app/models/pokemonList';
import { PokemonService } from 'src/app/services/pokemon.service';
import { concatMap, forkJoin, Observable, tap } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.page.html',
  styleUrls: ['./pokemon.page.css']
})
export class PokemonPage {

  constructor (private readonly pokemonService:PokemonService) {

  }

  min?: string = undefined;
  max?: string = undefined;
  next?: string = undefined;
  prev?: string = undefined;
  loadedPokemons : LoadedPokemon[] = [];

  ngOnInit():void {
    this.getData("");
  }

  nextPage() {
    if (this.next) {
      this.getData(this.next);
    }
  }

  prevPage() {
    if (this.prev) {
      this.getData(this.prev);
    }
  }

  getData(url: string) {
    this.pokemonService.getPokemon(url).subscribe((data) => {
      if (data.next != null) {
        this.next = data.next;
        this.max = data.next.split("=")[1].split("&")[0];
      }

      if (data.previous != null) {
        this.prev = data.previous;
        this.min = String(parseInt(data.previous.split("=")[1].split("&")[0]) + 10);
      }

      let pokemonList : LoadedPokemon[] = [];
      
      for (const pokemon of data.results) {
        let split : string[] = pokemon.url.split("/");
        let id : number = parseInt(split[split.length - 2]);

        pokemonList.push({
          id: id,
          name: pokemon.name,
          avatar: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`
        });
    
      }

      this.loadedPokemons = pokemonList;
    });
  }
}
