import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CollectedPokemon, User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage {
  constructor (private userService : UserService, private readonly router:Router) {}

  collectedPokemons : CollectedPokemon[] = this.userService.pokemons;

  get user():User{
    return this.userService.user
  }

  doLogOut() {
    this.userService.logOut();
    this.router.navigate(["/"])
  }

}