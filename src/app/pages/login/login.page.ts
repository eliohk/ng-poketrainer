import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { StorageKey } from 'src/app/enum/storageKey.enum';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { StorageUtil } from 'src/app/utils/storage.util';
import {  Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage implements OnInit{
  constructor (private readonly userService: UserService, private readonly router:Router) {}
  ngOnInit(): void {
  }

  public login(form: NgForm): void {
    
    this.userService.getUser(form.value.username).subscribe((data)=>{
      if (!(data.length > 0)) {
          this.userService.createUser(form.value.username).subscribe((user) => {
          this.userService.id = user.id;
          this.userService.userName = user.username;
          this.userService.userPokemons = user.pokemons;
          this.router.navigate(['pokemon'])
        })  
      } else{
        this.userService.id = data[0].id;
        this.userService.userName = data[0].username;
        this.userService.userPokemons = data[0].pokemons;
        this.router.navigate(['pokemon'])

      }
    }, (error) => {
      console.log(error);
    })
  }

}
