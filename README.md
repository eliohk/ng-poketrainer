# ng-poketrainer

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

This repository contains the deliverables for assignment 3 in the front-end module at Noroff Accelerate. The assignment asks for us to build a Pokémon Trainer web application using the Angular Framework. We have freedom to be as creative as we wish, as long as it meets the minimum requirements described in appendix A.

#### Web application link (through Vercel)

https://ng-poketrainer.vercel.app/

#### IMPORTANT DISCLAIMER
Note that the first time a user attempts to authorize (log in), the API may use half a minute to respond. This is due to the fact that glitch has to reactivate the hosted API.

In addition, the API-key was saved in environment.ts.

#### Figma
Link to figma can be found here:

https://www.figma.com/file/RtAqjXARAfm5Pn7bCBTNXp/SuperComponentTreeWithFatimaAndKoie?t=2faWi3LnGgasLCWw-1

In addition, a .pdf can be found in the root directory.

## Table of Contents

- [ng-poketrainer](#ng-poketrainer)
      - [IMPORTANT DISCLAIMER](#important-disclaimer)
  - [Table of Contents](#table-of-contents)
  - [Install](#install)
  - [Usage](#usage)
  - [API](#api)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Install

```
• NPM/Node.js (LTS – Long Term Support version)
• Angular CLI
• Visual Studio Code Text Editor/ IntelliJ
• Browser Developer Tools for testing and debugging
   o Angular Dev Tools
• Git
```

## Usage

```
- Clone repository
- Navigate to root folder with terminal
- Execute "npm i" to download dependencies
- Execute "ng serve" or "npm start" to host application
- open localhost:4200 in a browser
```

## API
Template:

https://github.com/dewald-els/noroff-assignment-api

Note that we changed the API data object a bit to accomodate the requirements in Appendix A. The pokemon list for each user is now following this template:

```
{
    id: string,
    username: string
}
```

This change was made due to the fact that Appendix A required a representation of both name and avatar of the pokemon. 

API URL:

https://snapdragon-reliable-badge.glitch.me/trainers

## Maintainers

[@Fatima](https://gitlab.com/fatimaya)

[@Khoi](https://gitlab.com/eliohk)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2023 Fatima & Khoi
